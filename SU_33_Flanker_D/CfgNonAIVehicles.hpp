class CfgNonAIVehicles
{
	class ProxyDriver;
	class ProxyWeapon;
	class ProxyKH_29: ProxyWeapon
	{
		model="\SU_33_Flanker_D\wep\KH-29";
		simulation="maverickweapon";
	};
	class ProxyR73_proxy: ProxyWeapon
	{
		model="\SU_33_Flanker_D\wep\R73_proxy";
		simulation="maverickweapon";
	};
	class ProxyR77_proxy: ProxyWeapon
	{
		model="\SU_33_Flanker_D\wep\R77_proxy";
		simulation="maverickweapon";
	};
	class ProxyR27RE_proxy: ProxyWeapon
	{
		model="\SU_33_Flanker_D\wep\R27RE_proxy";
		simulation="maverickweapon";
	};
	class Proxyfab250: ProxyWeapon
	{
		model="\SU_33_Flanker_D\wep\fab250";
		simulation="maverickweapon";
	};
	class ProxyFAB500: ProxyWeapon
	{
		model="\SU_33_Flanker_D\wep\FAB500";
		simulation="maverickweapon";
	};
	class Proxys8_launcher_aircraft: ProxyWeapon
	{
		model="\SU_33_Flanker_D\wep\s8_launcher_aircraft";
		simulation="maverickweapon";
	};
};
